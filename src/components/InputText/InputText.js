import React, { Component } from 'react'

import styles from './InputText.scss'

export default class InputText extends Component {
  render () {
    return (
      <div className={styles['input-text']}>
        <input {...this.props} />
      </div>
    )
  }
}
