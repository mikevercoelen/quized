import React, { Component, PropTypes } from 'react'

import styles from './Page.scss'

export default class Page extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props
    return (
      <div className={'container ' + styles['page']}>{children}</div>
    )
  }
}
