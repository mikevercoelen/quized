import React, { Component, PropTypes } from 'react'

import { propFormat as answerPropFormat } from 'components/Answer/Answer'
import AnswerList from 'components/AnswerList/AnswerList'

import styles from './QuestionForm.scss'

export default class QuestionForm extends Component {
  static propTypes = {
    question: PropTypes.shape({
      id: PropTypes.string.isRequired,
      answers: PropTypes.arrayOf(answerPropFormat).isRequired
    }).isRequired,
    onSelectAnswer: PropTypes.func.isRequired
  };

  render () {
    const {
      question: { answers },
      onSelectAnswer
    } = this.props

    return (
      <div className={styles['question-form']}>
        <AnswerList answers={answers} onSelectAnswer={onSelectAnswer} />
      </div>
    )
  }
}
