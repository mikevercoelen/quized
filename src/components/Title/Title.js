import React, { Component, PropTypes } from 'react'

import styles from './Title.scss'

export default class Title extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props
    return (
      <h1 className={styles['title']}>{children}</h1>
    )
  }
}
