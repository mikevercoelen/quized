import React, { Component, PropTypes } from 'react'

import styles from './Form.scss'

export default class Form extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired
  };

  render () {
    const { children, onSubmit } = this.props

    return (
      <form onSubmit={onSubmit} className={styles['form']}>{children}</form>
    )
  }
}
