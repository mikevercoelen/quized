import React, { Component, PropTypes } from 'react'

import styles from './Center.scss'

export default class Center extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props
    return (
      <div className={styles['center']}>
        {children}
      </div>
    )
  }
}
