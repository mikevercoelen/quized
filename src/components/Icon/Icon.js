import React, { Component, PropTypes } from 'react'

import styles from './Icon.scss'

import * as icons from 'icons'

export default class Icon extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired
  };

  render () {
    const { name } = this.props

    const icon = icons[name]

    if (icon === undefined) {
      console.error('Unknown icon: ' + name + ' supported icons: ', icons)
      return
    }

    const style = {
      fontFamily: icon.fontName
    }

    return (
      <div className={styles['icon']} style={style} data-icon={icon['unicode']}></div>
    )
  }
}
