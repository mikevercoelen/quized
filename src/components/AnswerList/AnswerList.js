import React, { Component, PropTypes } from 'react'

import Answer, { propFormat as answerPropFormat } from 'components/Answer/Answer'

import styles from './AnswerList.scss'

export default class AnswerList extends Component {
  static propTypes = {
    answers: PropTypes.arrayOf(answerPropFormat).isRequired,
    onSelectAnswer: PropTypes.func.isRequired
  };

  sortAnswers (answers) {
    return answers.sort((a, b) => a.order - b.order)
  };

  renderAnswerList = () => {
    const { answers, onSelectAnswer } = this.props
    const sortedAnswers = this.sortAnswers(answers)
    return sortedAnswers.map((answer, index) => (
      <li key={index} className={styles['answer-list__item']}>
        <Answer answer={answer} onSelect={onSelectAnswer} />
      </li>
    ))
  };

  render () {
    const answerList = this.renderAnswerList()
    return (
      <ul className={styles['answer-list']}>
        {answerList}
      </ul>
    )
  }
}
