import React, { Component, PropTypes } from 'react'

import styles from './PageHeader.scss'

import Logo from 'components/Logo/Logo'

export default class PageHeader extends Component {
  static propTypes = {
    label: PropTypes.node,
    controls: PropTypes.node
  };

  render () {
    const { label, controls } = this.props

    return (
      <div className={styles['page-header']}>
        <div className={styles['page-header__logo']}>
          <Logo />
        </div>
        <div className={styles['page-header__label']}>
          {label}
        </div>
        <div className={styles['page-header__controls']}>
          {controls}
        </div>
      </div>
    )
  }
}
