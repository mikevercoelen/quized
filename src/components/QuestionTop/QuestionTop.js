import React, { Component, PropTypes } from 'react'

import SubText from 'components/SubText/SubText'
import Title from 'components/Title/Title'

import styles from './QuestionTop.scss'

export default class QuestionTop extends Component {
  static propTypes = {
    question: PropTypes.shape({
      title: PropTypes.string.isRequired,
      number: PropTypes.number.isRequired
    })
  };

  render () {
    const {
      question: { title, number }
    } = this.props
    return (
      <div className={styles['question-top']}>
        <SubText>Vraag {number}.</SubText>
        <Title>{title}</Title>
      </div>
    )
  }
}
