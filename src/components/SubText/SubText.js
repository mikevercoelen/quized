import React, { Component, PropTypes } from 'react'

import styles from './SubText.scss'

export default class SubText extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props
    return (
      <p className={styles['sub-text']}>{children}</p>
    )
  }
}
