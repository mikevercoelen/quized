import React, { Component, PropTypes } from 'react'

import styles from './PageTop.scss'

export default class PageTop extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  render () {
    const { children } = this.props
    return (
      <div className={styles['page-top']}>{children}</div>
    )
  }
}
