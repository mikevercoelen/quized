import React, { Component, PropTypes } from 'react'

import styles from './FormError.scss'

export default class FormError extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired
  };

  render () {
    const {
      field: { touched, error }
    } = this.props

    return (
      <div className={styles['form-error']}>
        {touched && error && <div>{error}</div>}
      </div>
    )
  }
}
