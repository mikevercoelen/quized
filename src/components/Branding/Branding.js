import React, { Component } from 'react'

import styles from './Branding.scss'

export default class Branding extends Component {
  render () {
    return (
      <div className={styles['branding']}>
        <h1 className={styles['branding__title']}>
          Quizert
        </h1>
        <h2 className={styles['branding__sub-title']}>
          Hoe <strong>slim</strong> ben jij?
        </h2>
      </div>
    )
  }
}
