import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'

import styles from './Button.scss'

export default class Button extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    kind: PropTypes.oneOf(['primary']).isRequired
  };

  render () {
    const { kind, children } = this.props

    const cx = classNames({
      [styles['button']]: true,
      [styles[`button--${kind}`]]: true
    })

    return (
      <button className={cx} {...this.props}>{children}</button>
    )
  }
}
