import { Link } from 'react-router'
import React, { Component } from 'react'

import styles from './Logo.scss'

export default class Logo extends Component {
  render () {
    return (
      <Link to='/'>
        <h2 className={styles['logo']}>Quizert</h2>
      </Link>
    )
  }
}
