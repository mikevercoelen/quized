import React, { Component, PropTypes } from 'react'

import styles from './FormElement.scss'

export default class FormElement extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props
    return (
      <div className={styles['form-element']}>
        {children}
      </div>
    )
  }
}
