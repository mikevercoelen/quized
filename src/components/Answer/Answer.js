import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'

import styles from './Answer.scss'

export const propFormat = PropTypes.shape({
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired
})

export default class Answer extends Component {
  static propTypes = {
    answer: propFormat.isRequired,
    onSelect: PropTypes.func.isRequired
  };

  handleSelect = (event) => {
    const { answer, onSelect } = this.props
    onSelect(answer)
  };

  render () {
    const {
      answer: { label, selected }
    } = this.props

    const cx = classNames({
      [styles['answer']]: true,
      [styles[`answer--selected`]]: selected
    })

    return (
      <button className={cx} onClick={this.handleSelect}>{label}</button>
    )
  }
}
