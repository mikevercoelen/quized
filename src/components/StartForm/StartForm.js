import React, { Component, PropTypes } from 'react'
import { reduxForm } from 'redux-form'

import Form from 'components/Form/Form'
import FormElement from 'components/FormElement/FormElement'
import InputText from 'components/InputText/InputText'
import Button from 'components/Button/Button'
import Center from 'components/Center/Center'

import styles from './StartForm.scss'

const validate = values => {
  const errors = {}

  if (!values.fullName) {
    errors.fullName = 'Vul je volledige naam in.'
  }

  return errors
}

class StartForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired
  };

  render () {
    const {
      fields: { fullName },
      handleSubmit,
      submitting
    } = this.props

    const hasEnteredFullName = fullName.valid
    const disableSubmit = (submitting || !hasEnteredFullName)

    return (
      <div className={styles['start-form']}>
        <Form onSubmit={handleSubmit}>
          <FormElement>
            <InputText type='text' placeholder='Volledige naam' autoFocus {...fullName} />
          </FormElement>
          <FormElement>
            <Center>
              <Button kind='primary' type='submit' disabled={disableSubmit}>Start de quiz</Button>
            </Center>
          </FormElement>
        </Form>
      </div>
    )
  }
}

export default reduxForm({
  form: 'start',
  fields: ['fullName'],
  validate
})(StartForm)
