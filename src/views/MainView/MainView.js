import { connect } from 'react-redux'
import React, { Component, PropTypes } from 'react'
import { start } from 'redux/modules/quiz'
import { routeActions } from 'redux-simple-router'

import Page from 'components/Page/Page'
import PageHeader from 'components/PageHeader/PageHeader'
import PageTop from 'components/PageTop/PageTop'
import Branding from 'components/Branding/Branding'
import StartForm from 'components/StartForm/StartForm'

const mapStateToProps = (state) => {
  const { quiz } = state

  return {
    isLoaded: quiz.isLoaded,
    isLoading: quiz.isLoading,
    currentQuestion: quiz.currentQuestion
  }
}

class MainView extends Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    isLoaded: PropTypes.bool.isRequired,
    push: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
    currentQuestion: PropTypes.object
  };

  handleSubmit = (form) => {
    this.props.start(form.fullName)
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.isLoaded && nextProps.isLoaded === true) {
      this.props.push('/question/' + nextProps.currentQuestion.id)
    }
  };

  render () {
    return (
      <Page>
        <PageHeader />
        <PageTop>
          <Branding />
        </PageTop>
        <StartForm onSubmit={this.handleSubmit} />
      </Page>
    )
  }
}

export default connect(
  mapStateToProps,
  {
    start,
    push: routeActions.push
  }
)(MainView)
