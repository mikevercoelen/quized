// import { connect } from 'react-redux'
import React, { Component } from 'react'

import Page from 'components/Page/Page'
import PageHeader from 'components/PageHeader/PageHeader'
import PageTop from 'components/PageTop/PageTop'
import Center from 'components/Center/Center'
import Title from 'components/Title/Title'

export default class MainView extends Component {
  render () {
    const pageHeaderLabel = (<p>Totaal score</p>)

    return (
      <Page>
        <PageHeader label={pageHeaderLabel} />
        <PageTop>
          <Center>
            <Title>Je slimheid is bepaald</Title>
          </Center>
        </PageTop>
      </Page>
    )
  }
}
