import React, { Component, PropTypes } from 'react'

import Page from 'components/Page/Page'
import PageHeader from 'components/PageHeader/PageHeader'
import PageTop from 'components/PageTop/PageTop'

import QuestionTop from 'components/QuestionTop/QuestionTop'
import QuestionForm from 'components/QuestionForm/QuestionForm'

// const mapStateToProps = (state) => {
//   const { quiz } = state
//
//   return {
//     currentQuestion: quiz.currentQuestion
//   }
// }
//
// class QuestionView extends Component {
//   static propTypes = {
//     question: PropTypes.object
//   };
//
//   componentWillMount = () => {
//
//   };
//
//   render () {
//
//   }
// }
//
// export default connect(
//   mapStateToProps,
//   {
//
//   }
// )(QuestionView)

export default class QuestionView extends Component {


  handleSelectAnswer = (question) => {
    console.log(question)
  };

  get pageHeaderControls () {
    return (
      <div>lol</div>
    )
  }

  get pageHeaderLabel () {
    return (
      <p>Vraag 1 van de 5</p>
    )
  }

  render () {
    const question = {
      id: '1',
      nextId: 2,
      number: 1,
      title: 'Wat is je favoriete versioning tool?',
      answers: [{
        id: '1',
        order: 1,
        label: 'SVN',
        selected: false
      }, {
        id: '2',
        order: 2,
        label: 'GIT',
        selected: false
      }, {
        id: '3',
        order: 3,
        label: 'Mercurial',
        selected: true
      }, {
        id: '4',
        order: 4,
        label: 'CVS',
        selected: false
      }, {
        id: '5',
        order: 5,
        label: 'HUHH?',
        selected: false
      }]
    }

    return (
      <Page>
        <PageHeader label={this.pageHeaderLabel} controls={this.pageHeaderControls} />
        <PageTop>
          <QuestionTop question={question} />
        </PageTop>
        <QuestionForm question={question} onSelectAnswer={this.handleSelectAnswer} />
      </Page>
    )
  }
}
