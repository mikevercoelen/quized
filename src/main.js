import React from 'react'
import { render } from 'react-dom'
import { useRouterHistory } from 'react-router'
import { createHistory } from 'history'
import Root from './containers/Root'
import configureStore from './redux/configureStore'
import getRoutes from './routes'

import iconFont from 'icons-loader'
import styleInject from 'style-inject'

styleInject(iconFont.css)

const historyConfig = { basename: __BASENAME__ }
const history = useRouterHistory(createHistory)(historyConfig)

const initialState = window.__INITIAL_STATE__
const store = configureStore({ initialState, history })

getRoutes(store, (routes) => {
  render(
    <Root history={history} routes={routes} store={store} />,
    document.getElementById('root')
  )
})
