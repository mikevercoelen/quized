import { combineReducers } from 'redux'
import { routeReducer as router } from 'redux-simple-router'
import { reducer as form } from 'redux-form'

import quiz from './modules/quiz'

export default combineReducers({
  router,
  form,
  quiz
})
