import request from 'superagent'
import { handleActions, createAction } from 'redux-actions'

const API_URL = 'http://localhost:5000'

export const QUIZ_START_REQUEST = 'QUIZ_START_REQUEST'
export const QUIZ_START_RECEIVE = 'QUIZ_START_RECEIVE'
export const QUIZ_START_ERROR = 'QUIZ_START_ERROR'

export const QUIZ_UPDATE_REQUEST = 'QUIZ_UPDATE_REQUEST'
export const QUIZ_UPDATE_RECEIVE = 'QUIZ_UPDATE_RECEIVE'
export const QUIZ_UPDATE_ERROR = 'QUIZ_UPDATE_ERROR'

const startRequest = createAction(QUIZ_START_REQUEST)
const startReceive = createAction(QUIZ_START_RECEIVE)
const startError = createAction(QUIZ_START_ERROR)

const updateRequest = createAction(QUIZ_UPDATE_REQUEST)
const updateReceive = createAction(QUIZ_UPDATE_RECEIVE)
const updateError = createAction(QUIZ_UPDATE_ERROR)

export function update (quiz) {
  return dispatch => {
    dispatch(updateRequest())

    request
      .post(API_URL + '/quiz/' + quiz.id)
      .send(quiz)
      .end((error, response) => {
        if (error) {
          return dispatch(updateError(error))
        }

        dispatch(updateReceive())
      })
  }
}

export function start (fullName) {
  return dispatch => {
    dispatch(startRequest())

    request
      .post(API_URL + '/quiz')
      .send({ fullName })
      .end((error, response) => {
        if (error) {
          return dispatch(startError(error))
        }

        const { id, questions, fullName } = response.body
        const currentQuestion = questions[0]

        dispatch(startReceive({
          id,
          questions,
          fullName,
          currentQuestion
        }))
      })
  }
}

const initialState = {
  isLoaded: false
}

export default handleActions({
  [QUIZ_START_REQUEST]: (state, action) => ({
    ...state,
    isLoading: true
  }),
  [QUIZ_START_RECEIVE]: (state, action) => ({
    ...state,
    isLoading: false,
    isLoaded: true,
    id: action.payload.id,
    fullName: action.payload.fullName,
    questions: action.payload.questions,
    currentQuestion: action.payload.currentQuestion
  }),
  [QUIZ_START_ERROR]: (state, action) => ({
    ...state,
    isLoading: false,
    isLoaded: false,
    error: action.error
  }),
  [QUIZ_UPDATE_REQUEST]: (state, action) => ({
    ...state,
    isUpdating: true
  }),
  [QUIZ_UPDATE_RECEIVE]: (state, action) => ({
    ...state,
    isUpdating: false
  }),
  [QUIZ_UPDATE_ERROR]: (state, action) => ({
    ...state,
    isUpdating: false,
    error: action.error
  })
}, initialState)
