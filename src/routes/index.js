import React from 'react'
import { Route, Redirect, IndexRoute } from 'react-router'

// layout
import CoreLayout from 'layouts/CoreLayout/CoreLayout'

// views
import MainView from 'views/MainView/MainView'
import QuestionView from 'views/QuestionView/QuestionView'
import ScoreView from 'views/ScoreView/ScoreView'

export default (store, callback) => {
  const isQuizStarted = (nextState, replace, callback) => {
    const { quiz } = store.getState()

    if (!quiz.isLoaded) {
      replace('/')
    }

    callback()
  }

  callback((
    <Route path='/' component={CoreLayout}>
      <IndexRoute components={MainView} />

      <Route onEnter={isQuizStarted}>
        <Route path='question/:questionId' component={QuestionView} />
        <Route path='score' component={ScoreView} />
      </Route>

      <Redirect from='*' to='/' />
    </Route>
  ))
}
