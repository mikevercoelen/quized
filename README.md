# Brightin's - Quizert

## Features
- See [usecases.pdf](usecases.pdf)

## Coding styles / guidelines
- Based on [Redux starter kit @ cb28dd357f666e9bcd2848239afc44e2cb8bbc90](https://github.com/davezuko/react-redux-starter-kit/tree/cb28dd357f666e9bcd2848239afc44e2cb8bbc90)
- [Standardjs](http://standardjs.com)
- No semicolons

## Designs
Designs can be found in `/designs`. Used Sketch 3.4.3.

## Requirements
- node `^4.2.0`
- npm `^3.0.0`

## Getting started

```shell
npm install
npm start
```

## Development

```shell
npm start
```

## Distribution / deployment

Runs linter, tests, on success compiles to `/dist` folder.

```shell
npm run deploy
```

Compiles the application to `/dist` folder.

```shell
npm run compile
```

## More information
See [Redux starter kit](https://github.com/davezuko/react-redux-starter-kit/tree/cb28dd357f666e9bcd2848239afc44e2cb8bbc90)
